PostIt
======

PostIt is a work-in-progress implementation of a SCRUM
board, as used inside Vaadin.

Planned features include
 * Tickets overview
 * Ticket details view
 * Ticket management (being able to create and destroy tickets, as well as move tickets from one column to another)
 * Presentation mode, where control over display is given to an external session
   * That session is ideally controlled on a tablet
   * Tablet control is used to select and highlight ticket under discussion
 * Ticket details - being able to add arbitrary amounts of data and discussion to a ticket
 * Concurrent multi-user access